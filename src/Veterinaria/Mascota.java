/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Veterinaria;

import java.awt.Image;
import java.util.ArrayList;

/**
 *
 * @author oscar
 */
public class Mascota {
    String nombre;
    Dueno dueno;
    String foto;
    String observaciones;
    Integer ranking;

    public Mascota(String nombre, Dueno dueno, String foto, String observaciones, Integer ranking) {
        this.nombre = nombre;
        this.dueno = dueno;
        this.foto = foto;
        this.observaciones = observaciones;
        this.ranking = ranking;
    }

    @Override
    public String toString() {
        return "Mascota{" + "nombre=" + nombre + ", dueno=" + dueno + ", foto=" + foto + ", observaciones=" + observaciones + ", ranking=" + ranking + '}';
    }
    
     public static Boolean mascotaRepetido(ArrayList<Mascota> mascotas) {
        Integer repetido = 0;
        for (int i = 0; i < mascotas.size(); i++) {
            for (int j = 0; j < mascotas.size(); j++) {
                
                if (mascotas.get(i).nombre.equals(mascotas.get(j).nombre) &&
                        !mascotas.get(i).equals(mascotas.get(j))) {
                    repetido+=1;
                }
            }
        }
        return repetido > 1;  
    }
}
