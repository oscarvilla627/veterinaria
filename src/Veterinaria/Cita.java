/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Veterinaria;

import java.util.Date;

/**
 *
 * @author oscar
 */
public class Cita {
    String nombreMascota;
    Date fecha;
    Date hora;
    String observaciones;

    public Cita(String nombreMascota, Date fecha, Date hora, String observaciones) {
        this.nombreMascota = nombreMascota;
        this.fecha = fecha;
        this.hora = hora;
        this.observaciones = observaciones;
    }

    @Override
    public String toString() {
        return "Cita{" + "nombreMascota=" + nombreMascota + ", fecha=" + fecha + ", hora=" + hora + ", observaciones=" + observaciones + '}';
    }
}
