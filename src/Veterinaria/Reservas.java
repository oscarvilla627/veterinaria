/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Veterinaria;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author oscar
 */
public class Reservas {
    String nombreMascota;
    Date fechaEntrada;
    Date fechaSalida;

    public Reservas(String nombreMascota, Date fechaEntrada, Date fechaSalida) {
        this.nombreMascota = nombreMascota;
        this.fechaEntrada = fechaEntrada;
        this.fechaSalida = fechaSalida;
    }

    @Override
    public String toString() {
        return "Reservas{" + "nombreMascota=" + nombreMascota + ", fechaEntrada=" + fechaEntrada + ", fechaSalida=" + fechaSalida + '}';
    }
   
}
