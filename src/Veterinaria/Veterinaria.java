/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Veterinaria;

import java.awt.image.BufferedImage;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.imageio.ImageIO;

/**
 *
 * @author oscar
 */
public class Veterinaria {
 
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParseException {
        // TODO code application logic here
        ArrayList<Mascota> mascotas = new ArrayList<>();
        ArrayList<Usuario> usuarios = new ArrayList<>();
        ArrayList<Reservas> reservas = new ArrayList<>();
        ArrayList<Cita> citas = new ArrayList<>();
    
       
        
        //Mascotas
        Dueno dueno1 = new Dueno("Oscar", "12344", "Cartago", "333333");
        Mascota mascota1 = new Mascota("Osiris", dueno1, "foto", "Rottwailer",3);
        Mascota mascota2 = new Mascota("Antara", dueno1, "foto", "Rottwailer",3);
        Mascota mascota3 = new Mascota("Navas", dueno1, "foto", "Rottwailer",3);
        Mascota mascota4 = new Mascota("Osiris", dueno1, "foto", "Rottwailer",3);
        mascotas.add(mascota1);
        mascotas.add(mascota2);
        mascotas.add(mascota3);
        mascotas.add(mascota4);
        
        Boolean repetido1 = Mascota.mascotaRepetido(mascotas);
        System.out.println("Las mascotas estan repetidas:" + repetido1);
        
        //Usuarios
        Usuario usuario1 = new Usuario("Vet1", "345", "123", "San Jose", "Activo", "Doctor");
        Usuario usuario2 = new Usuario("Vet2", "123", "123", "San Jose", "Activo", "Doctor");
        Usuario usuario3 = new Usuario("Vet3", "891", "123", "San Jose", "Activo", "Doctor");
        usuarios.add(usuario1);
        usuarios.add(usuario2);
        usuarios.add(usuario3);
        
        Boolean repetido = Usuario.usuarioRepetido(usuarios);
        System.out.println("Los usuarios estan repetidos:" + repetido);
        
        //Reservas
        Date date1 = new Date();
        String myDateString = "13:24:40";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Date hora = sdf.parse(myDateString);
        
        Reservas reserva1 = new Reservas("Osiris", date1, hora);
        Reservas reserva2 = new Reservas("Navas", date1, hora);
        Reservas reserva3 = new Reservas("Orion", date1, hora);
        reservas.add(reserva1);
        reservas.add(reserva2);
        reservas.add(reserva3);
        
        //Citas
        Cita cita1 = new Cita("Navas", date1, date1, "Esta enferma");
        Cita cita2 = new Cita("Osiris", date1, date1, "Esta enferma 1");
        Cita cita3 = new Cita("Orion", date1, date1, "Esta enferma 2");
        citas.add(cita1);
        citas.add(cita2);
        citas.add(cita3);
        
        // Imprimir 
        System.out.println(mascotas);
        System.out.println(usuarios);
        System.out.println(reservas);
        System.out.println(citas);
    }
}
