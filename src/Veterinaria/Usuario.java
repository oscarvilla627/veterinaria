/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Veterinaria;

import com.sun.org.apache.xpath.internal.operations.Bool;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author oscar
 */
public class Usuario {
    String nombre;
    String cedula;
    String telefono;
    String direccion;
    String estado;
    String rol;

    public Usuario(String nombre, String cedula, String telefono, String direccion, String estado, String rol) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.telefono = telefono;
        this.direccion = direccion;
        this.estado = estado;
        this.rol = rol;
    }

    @Override
    public String toString() {
        return "Usuario{" + "nombre=" + nombre + ", cedula=" + cedula + ", telefono=" + telefono + ", direccion=" + direccion + ", estado=" + estado + ", rol=" + rol + '}';
    }
    
    public static Boolean usuarioRepetido(ArrayList<Usuario> usuarios) {
        Integer repetido = 0;
        for (int i = 0; i < usuarios.size(); i++) {
            for (int j = 0; j < usuarios.size(); j++) {
                
                if (usuarios.get(i).cedula.equals(usuarios.get(j).cedula) &&
                        !usuarios.get(i).equals(usuarios.get(j))) {
                    repetido+=1;
                }
            }
        }
        return repetido > 1;  
    }
    
}
